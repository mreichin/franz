from __future__ import absolute_import, division, print_function

import os
from datetime import datetime
import pandas as pd
from keras.callbacks import Callback, EarlyStopping
from sacred.observers import MongoObserver
from sklearn.compose import ColumnTransformer
from sklearn.dummy import DummyRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, RationalQuadratic, ConstantKernel, DotProduct, Matern
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import LinearRegression, Ridge, \
    RidgeCV, Lasso, SGDRegressor, LassoCV
from sklearn.model_selection import KFold, cross_val_score, train_test_split, GridSearchCV
from sklearn.neural_network import MLPRegressor
from sklearn.pipeline import Pipeline
from sklearn.svm import SVR, LinearSVR
from sklearn.tree import DecisionTreeRegressor
from keras.wrappers.scikit_learn import KerasRegressor

from utils import get_feature_labels, plot_top_feature_importances, \
    log_and_plot_feature_importances, print_data_info, prepare_numerical_transformers, \
    prepare_categorial_transformers, \
    plot_top_weights, log_and_plot_weights, get_num_input_neurons, KerasTwoLayerFFRegressor, plot_target_distribution, \
    plot_and_log_permutation_feature_importance

from sacred import Experiment
import numpy as np

####################################      CONFIGURATION      ###########################################################

ex = Experiment()
ex.observers.append(MongoObserver.create(db_name="experiments"))


@ex.config
def cfg():
    REGRESSOR = 'DecisionTreeRegressor'
    GRIDSEARCH = False
    PFIR = None
    NROWS = None  # 'None': does read in all rows
    DROP_ROWS = 'any'  # 'all' only completely empty raws, 'any' if at least one is empty, '11'(thres): With 11 filled at least
    ENCODER = 'ohe'  # 'le': LabelEncoder; 'ohe': OneHotEncoder, NOTE: Use 'le' only for tree based models
    CROSS_VALIDATION = None

    SOURCE_DATA = "source_file_name.csv"
    TRAIN_LABELS_CATEGORIAL = [label for label in
                               ["featurename3", "featurename4", "featurename5"]
                               # use this to exclude one of the train labels
                               if label not in ["featurename3"]]
    #                              9            10          11              12
    TRAIN_LABELS_NUMERICAL = [label for label in ["featurename6",  "featurename7", "featurename8"]
                              # use this to exclude one of the train labels
                              if label not in ["featurename7"]]
    GROUPS = []

def get_scikit_regressor(REGRESSOR):
    if REGRESSOR == "DummyRegressor":
        return DummyRegressor(strategy='mean')
    elif REGRESSOR == "LinearRegression":
        return LinearRegression()
    elif REGRESSOR == "Ridge":
        return Ridge()
    elif REGRESSOR == "RidgeCV":
        return RidgeCV()
    elif REGRESSOR == "Lasso":
        return Lasso()
    elif REGRESSOR == "LassoCV":
        return LassoCV()
    elif REGRESSOR == 'KernelRidge':
        return KernelRidge()
    elif REGRESSOR == 'SVR':
        return SVR()
    elif REGRESSOR == 'LinearSVR':
        return LinearSVR(C=100)
    elif REGRESSOR == 'SGDRegressor':
        return SGDRegressor()
    elif REGRESSOR == 'DecisionTreeRegressor':
        return DecisionTreeRegressor()
    elif REGRESSOR == 'RandomForestRegressor':
        return RandomForestRegressor()
    elif REGRESSOR == 'GaussianProcessRegressor':
        return GaussianProcessRegressor()
    elif REGRESSOR == 'MLPRegressor':
        return MLPRegressor()


def get_keras_regressor(REGRESSOR, num_in):
    if REGRESSOR == 'KerasTwoLayerFFRegressor':
        return KerasRegressor(KerasTwoLayerFFRegressor(num_in=num_in),
                              epochs=1500, batch_size=256, verbose=0)


def get_params(REGRESSOR, NROWS):
    if REGRESSOR == "LinearRegression":
        return {}
    elif REGRESSOR == "Ridge":
        return {'ml-step__alpha': [1, 10, 20, 1e-3, 1e-2, 1e-1]}
    elif REGRESSOR == "Lasso":
        return {'ml-step__alpha': [1, 10, 20, 1e-3, 1e-2, 1e-1]}
    elif REGRESSOR == 'KernelRidge':
        return {'ml-step__alpha': [1, 10, 20, 1e-3, 1e-2, 1e-1],
                'ml-step__kernel': [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                                    1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
                                    ConstantKernel(0.1, (0.01, 10.0)) * (
                                                DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
                                    1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0), nu=1.5)]}
    elif REGRESSOR == 'SVR':
        return {'ml-step__C': [0.001, 0.1, 10, 100, 10e5]}
    elif REGRESSOR == 'LinearSVR':
        return {'ml-step__C': [0.001, 0.1, 10, 100, 10e5]}
    elif REGRESSOR == 'SGDRegressor':
        return {'ml-step__alpha': [1, 10, 1e-4, 1e-3, 1e-2, 1e-1]}
    elif REGRESSOR == 'DecisionTreeRegressor':
        return {'ml-step__max_depth': [50, None],
                'ml-step__max_features': ['auto', 'log2'],
                'ml-step__min_samples_leaf': [0.1, 0.4],
                'ml-step__min_samples_split': [0.1, 0.6]}
    elif REGRESSOR == 'GaussianProcessRegressor':
        return {'ml-step__alpha': [1, 10, 1e-3, 1e-1],
                'ml-step__kernel': [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                                    1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
                                    ConstantKernel(0.1, (0.01, 10.0)) * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
                                    1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0), nu=1.5)]}
    elif REGRESSOR == 'MLPRegressor':
        return {'ml-step__alpha': [1.0, 20.0, 1e-3, 1e-1],
                'ml-step__hidden_layer_sizes': [(NROWS//100, NROWS//500), (NROWS//10,)],
                'ml-step__learning_rate': ['constant', 'adaptive']}
    elif REGRESSOR == 'KerasTwoLayerFFRegressor':
        return {}


def get_keras_callbacks():
    class LogMetrics(Callback):
        def on_epoch_end(self, _, logs={}):
            ex.log_scalar("loss", float(logs.get('loss')))
            ex.log_scalar("mae", float(logs.get('mean_absolute_error')))
            ex.result = float(logs.get('mean_absolute_error'))
    return {'ml-step__callbacks': [LogMetrics(), EarlyStopping(monitor='loss', verbose=1)]}


@ex.automain
def run(TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, SOURCE_DATA, NROWS, DROP_ROWS,
        REGRESSOR, ENCODER, GRIDSEARCH, PFIR, CROSS_VALIDATION, GROUPS):

    # get run id
    RUN_ID = ex.observers[0].run_entry.get('_id')

    # set target from the name of the run
    if ex.observers[0].run_entry.get('experiment').get('name') != 'predict_numerical':
        TARGET = ex.observers[0].run_entry.get('experiment').get('name')
    else:
        raise ValueError('No target name provided as parameter of the experiment. Set parameter --name=TARGET_NAME')

    # define labels to load from csv
    labels = TRAIN_LABELS_CATEGORIAL + TRAIN_LABELS_NUMERICAL + [TARGET]

    # create folder where to store all plots and results
    folder = 'results/ID-' + str(RUN_ID) + '_' + TARGET + "_with_" + REGRESSOR + "_" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + '/'
    os.makedirs(folder, exist_ok=True)

    ####################################      LOAD AND SPLIT     #######################################################

    # get dataframe with pandas
    raw_dataset = pd.read_csv(SOURCE_DATA, dtype=str, nrows=NROWS, na_values="?", skipinitialspace=True,
                              encoding='iso-8859-1', usecols=labels)

    # print raw data statistics
    print_data_info(raw_dataset)

    # drop target missing
    cleaned_dataset = raw_dataset[raw_dataset[TARGET].notnull()]

    # select target
    X = cleaned_dataset.drop([TARGET], axis=1)
    Y = cleaned_dataset[TARGET]

    # plot target distribution
    plot_target_distribution(Y, ex, TARGET, folder)

    # split train an test
    X_train_raw, X_test, y_train_raw, y_test = train_test_split(X, Y, test_size=0.2, random_state=30)

    ## prepare train set
    raw_train_set = pd.concat([X_train_raw, y_train_raw], axis=1)

    # drop empty (any, all or below thresh)
    cleaned_train_set = raw_train_set.dropna(how=DROP_ROWS) if 'any' in DROP_ROWS or 'all' in DROP_ROWS \
        else raw_train_set.dropna(thresh=int(DROP_ROWS))

    # print cleaned train set statistics
    print_data_info(cleaned_train_set, cleaned=True)

    X_train = cleaned_train_set.drop([TARGET], axis=1)
    y_train = cleaned_train_set[TARGET]

    ####################################         PIPELINE        #######################################################

    # prepare categorial pipeline
    cat_transformers = prepare_categorial_transformers(ENCODER, TRAIN_LABELS_CATEGORIAL)

    # prepare numerical pipeline
    num_transformers = prepare_numerical_transformers(TRAIN_LABELS_NUMERICAL)

    # put together categorial and numerical transformers
    transformers = cat_transformers + num_transformers
    ct = ColumnTransformer(transformers=transformers)

    # get regressor
    if 'Keras' in REGRESSOR:
        num_input_neurons = get_num_input_neurons(ENCODER, TRAIN_LABELS_NUMERICAL, ct, X_train)
        regressor = get_keras_regressor(REGRESSOR, num_input_neurons)
    else:
        regressor = get_scikit_regressor(REGRESSOR)

    # print regressor params
    print('Params for ' + REGRESSOR)
    print(regressor.get_params())

    # building ML-pipeline
    ml_pipe = Pipeline([('transform', ct), ('ml-step', regressor)])

    ####################################     GRIDSEARCH      ###########################################################

    grid = None
    if GRIDSEARCH:
        grid = GridSearchCV(ml_pipe, param_grid=get_params(REGRESSOR, NROWS), cv=5)

    ####################################   TRAIN - PREDICT   ###########################################################

    # create fit_params for Keras classifier
    fit_params = get_keras_callbacks() if 'Keras' in REGRESSOR else {}

    if GRIDSEARCH:
        # train the ML-model (very time consuming!)
        grid.fit(X_train, y_train, **fit_params)

        # override unfitted pipe with fitted
        ml_pipe = grid.best_estimator_
        ct = ml_pipe.named_steps['transform']
    else:
        # train the ML-model (time consuming!)
        ml_pipe.fit(X_train, y_train, **fit_params)

    # predict
    predictions = ml_pipe.predict(X_test)
    print(predictions)

    ####################################   ANALYSE MODEL   ##########################################################

    # get estimator from pipe
    estimator = ml_pipe.named_steps['ml-step']

    # set categories
    categories = []
    if ENCODER == 'ohe' and hasattr(ct.named_transformers_['cat'].named_steps['ohe'], 'categories_'):
        categories = ct.named_transformers_['cat'].named_steps['ohe'].categories_

    # set feature labels
    feature_labels = get_feature_labels(ENCODER, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, categories)

    # analyse feature_importances_ (eg. DecitionTreeRegressor)
    if hasattr(estimator, 'feature_importances_'):
        # plot top
        plot_top_feature_importances(ex, TARGET, estimator, feature_labels, folder, REGRESSOR, ENCODER)
        # log and plot summed
        log_and_plot_feature_importances(ex, ENCODER, categories, estimator, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, TARGET, REGRESSOR, folder)

    # analyse feature importance by permutation (more reliable)
    if PFIR:
        plot_and_log_permutation_feature_importance(ml_pipe, X_test, y_test, feature_labels, ENCODER, ex, TARGET, REGRESSOR, folder,
                                                n_repeats=PFIR, n_jobs=1, groups=GROUPS.copy())

    # analyse coefs (weights) (eg. Ridge)
    if hasattr(estimator, 'coef_'):
        # plot weights
        plot_top_weights(ex, REGRESSOR, TARGET, estimator, feature_labels, folder, ENCODER)
        # log weights
        log_and_plot_weights(ex, ENCODER, TARGET, categories, estimator, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL,
                             REGRESSOR, folder, target_classes=[TARGET])

    ####################################       PRINT        ###########################################################

    # concat data and print to file
    test_results_with_top_prediction = np.concatenate((X_test.get(TRAIN_LABELS_CATEGORIAL), X_test.get(TRAIN_LABELS_NUMERICAL),
                                                        pd.DataFrame(y_test), predictions.reshape(-1, 1)), axis=1)
    out_header = np.concatenate((TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, ['SOLL_' + TARGET, 'PRED_' + TARGET]))
    np.savetxt(folder + TARGET + '_predictions.csv',
               np.append([out_header], test_results_with_top_prediction, axis=0), fmt='%s', delimiter=',')

    ####################################       EVALUTATE       ##########################################################

    if GRIDSEARCH:
        print("Best grid params: %s" % grid.best_params_)
        print("Best grid score on training data: %f" % grid.best_score_)
        return grid.best_score_
    elif "CV" in REGRESSOR:
        print("Best alpha using built-in cross validation: %f" % estimator.alpha_)
        score_cross = ml_pipe.score(X_train, y_train)
        print("Best score on training data using built-in cross validation: %f" % score_cross)
        test_score = ml_pipe.score(X_test, y_test)
        print("Score on test data: %f" % test_score)
        return score_cross
    else:
        train_score = ml_pipe.score(X_train, y_train)
        print("Score on training data: %f" % train_score)
        test_score = ml_pipe.score(X_test, y_test)
        print("Score on test data: %f" % test_score)
        if 'Keras' in REGRESSOR:
            print("Aborted cross validation since it is not supported for Keras")
            return test_score
        elif not CROSS_VALIDATION:
            return test_score
        elif train_score > 0.5:
            kf = KFold(n_splits=5, shuffle=True, random_state=123)
            # cross validation (very time consuming!)
            score_cross = cross_val_score(ml_pipe, X_train, y_train, cv=kf)
            print("Cross Validation scores: ")
            print(score_cross)
            print("Cross Validation scores: %f" % score_cross.mean())
            return score_cross.mean()
        else:
            print("Aborted cross validation since train score is only: %f" % train_score)
            return train_score


if __name__ == '__run__':
    run()
