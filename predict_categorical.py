from __future__ import absolute_import, division, print_function

import os
from datetime import datetime
import pandas as pd
from keras.callbacks import Callback, EarlyStopping
from keras.wrappers.scikit_learn import KerasClassifier

from sacred.observers import MongoObserver
from sklearn.compose import ColumnTransformer
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.dummy import DummyClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import RidgeClassifier, RidgeClassifierCV, LogisticRegressionCV
from sklearn.model_selection import KFold, cross_val_score, train_test_split, GridSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier, ExtraTreeClassifier
from imblearn.over_sampling import SMOTENC

from utils import get_feature_labels, plot_top_feature_importances, \
    log_and_plot_feature_importances, print_data_info, prepare_categorial_transformers, \
    prepare_numerical_transformers, \
    plot_top_weights, log_and_plot_weights, get_num_input_neurons, KerasTwoLayerFFClassifier, \
    plot_classification_results, plot_target_distribution, plot_confusion_matrix, plot_precision_recall_F1, \
    plot_and_log_permutation_feature_importance, FrequencyClassifier, print_change_rate

from sacred import Experiment
import numpy as np

####################################      CONFIGURATION      ###########################################################

ex = Experiment()
ex.observers.append(MongoObserver.create(db_name="experiments"))


@ex.config
def cfg():
    CLASSIFIER = 'RandomForestClassifier'
    ONE_VS_REST_CLASS = None
    GRIDSEARCH = None
    SMOTE = None
    PFIR = None
    NROWS = None  # 'None': does read in all rows
    DROP_ROWS = 'any'  # 'all' only completely empty raws, 'any' if at least one column is empty, '11'(thres): With 11 filled at least
    ENCODER = 'le'  # 'le': LabelEncoder; 'ohe': OneHotEncoder, NOTE: Use 'le' only for tree based models
    MIN_CLASS_SUPPORT = 5
    DROP_LOW_SUPPORTED = None
    CROSS_VALIDATION = None

    SOURCE_DATA = "source_file_name.csv"
    TRAIN_LABELS_CATEGORIAL = [label for label in ["featurename3", "featurename4", "featurename5"]
                              # use this to exclude one or more of the train labels
                              if label not in ["featurename3"]]
    TRAIN_LABELS_NUMERICAL = [label for label in ["featurename6",  "featurename7", "featurename8"]
                              # use this to exclude one or more of the train labels
                              if label not in ["featurename7"]]
    GROUPS = []


def get_scikit_classifier(CLASSIFIER):
    if CLASSIFIER == "DummyClassifier":
        return DummyClassifier(strategy="stratified")
    elif CLASSIFIER == "LinearSVC":
        return LinearSVC(multi_class='crammer_singer', verbose=1)
    elif CLASSIFIER == "LogisticRegression":
        return LogisticRegressionCV(multi_class='multinomial', solver='saga', verbose=1)
    elif CLASSIFIER == "LogisticRegressionCV":
        return LogisticRegressionCV(multi_class='multinomial', solver='saga', verbose=1)
    elif CLASSIFIER == "RidgeClassifierCV":
        return RidgeClassifierCV()
    elif CLASSIFIER == "RidgeClassifier":
        return RidgeClassifier()
    elif CLASSIFIER == "DecisionTreeClassifier":
        return DecisionTreeClassifier(random_state=0)
    elif CLASSIFIER == "MLPClassifier":
        return MLPClassifier(early_stopping=True, verbose=0)
    elif CLASSIFIER == "LinearDiscriminantAnalysis":
        return LinearDiscriminantAnalysis(solver='svd')
    elif CLASSIFIER == "RandomForestClassifier":
        return RandomForestClassifier(verbose=1)
    elif CLASSIFIER == "KNeighborsClassifier":
        return KNeighborsClassifier()
    elif CLASSIFIER == "ExtraTreeClassifier":
        return ExtraTreeClassifier()
    elif CLASSIFIER == "GradientBoostingClassifier":
        return GradientBoostingClassifier()
    elif CLASSIFIER == "GaussianNB":
        return GaussianNB()
    else:
        raise ValueError('Unknown scikit classifier.')


def get_keras_classifier(CLASSIFIER, num_in, num_out):
    if CLASSIFIER == 'KerasTwoLayerFFClassifier':
        return KerasClassifier(KerasTwoLayerFFClassifier(num_in=num_in, num_out=num_out),
                               epochs=2000, batch_size=256, verbose=0)
    else:
        raise ValueError('Unknown keras classifier.')


def get_params(CLASSIFIER, NROWS):
    if CLASSIFIER == "LinearSVC":
        return {'ml-step__C': [0.001, 0.1, 10, 100, 10e5]}
    elif CLASSIFIER == "LogisticRegression":
        return {}
    elif CLASSIFIER == "RidgeClassifier":
        return {'ml-step__alpha': [1, 10, 20, 1e-3, 1e-2, 1e-1]},
    elif CLASSIFIER == "DecisionTreeClassifier":
        return {'ml-step__max_depth': [50, None],
                'ml-step__max_features': ['auto', 'log2'],
                'ml-step__min_samples_leaf': [0.1, 0.4],
                'ml-step__min_samples_split': [0.1, 0.6]}
    elif CLASSIFIER == "MLPClassifier":
        return {'ml-step__alpha': [1.0, 20.0, 1e-3, 1e-1],
                'ml-step__hidden_layer_sizes': [(NROWS//100, NROWS//500), (NROWS//10,)],
                'ml-step__learning_rate': ['constant', 'adaptive']}
    elif CLASSIFIER == "LinearDiscriminantAnalysis":
        return {}
    elif CLASSIFIER == "RandomForestClassifier":
        return {'ml-step__max_depth': [50, None],
                'ml-step__max_features': ['auto', 'log2'],
                'ml-step__min_samples_leaf': [1, 4],
                'ml-step__min_samples_split': [2, 10],
                'ml-step__n_estimators': [10, 100, 300]}
    elif CLASSIFIER == "KNeighborsClassifier":
        return {}
    elif CLASSIFIER == "ExtraTreeClassifier":
        return {}
    if CLASSIFIER == 'KerasTwoLayerFFClassifier':
        return {}


def get_keras_callbacks():
    class LogMetrics(Callback):
        def on_epoch_end(self, _, logs={}):
            ex.log_scalar("loss", float(logs.get('loss')))
            ex.log_scalar("acc", float(logs.get('acc')))
            ex.result = float(logs.get('acc'))
    return {'ml-step__callbacks': [LogMetrics(), EarlyStopping(monitor='loss', verbose=1)]}


@ex.automain
def run(TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, SOURCE_DATA, NROWS, DROP_ROWS,
        CLASSIFIER, ENCODER, GRIDSEARCH, SMOTE, PFIR, MIN_CLASS_SUPPORT,
        CROSS_VALIDATION, DROP_LOW_SUPPORTED, ONE_VS_REST_CLASS, GROUPS):

    # get run id
    RUN_ID = ex.observers[0].run_entry.get('_id')

    # set target from the name of the run
    if ex.observers[0].run_entry.get('experiment').get('name') != 'predict_categorical':
        TARGET = ex.observers[0].run_entry.get('experiment').get('name')
    else:
        raise ValueError('No target name provided as parameter of the experiment. Set parameter --name=TARGET_NAME')

    # define labels to load from csv
    labels = TRAIN_LABELS_CATEGORIAL + TRAIN_LABELS_NUMERICAL + [TARGET]

    # create folder where to store all plots and results
    folder = 'results/ID-' + str(RUN_ID) + '_' + TARGET + "_with_" + CLASSIFIER + "_" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + '/'
    os.makedirs(folder, exist_ok=True)

    ####################################      LOAD AND SPLIT     #######################################################

    # get dataframe with pandas
    raw_dataset = pd.read_csv(SOURCE_DATA, dtype=str, nrows=NROWS, na_values="?", skipinitialspace=True,
                              encoding='iso-8859-1', usecols=labels)

    # print raw data statistics
    print_data_info(raw_dataset)

    # drop target missing
    cleaned_dataset = raw_dataset[raw_dataset[TARGET].notnull()]

    # analyse change rate
    print_change_rate(TARGET, cleaned_dataset)

    # use One-Vs-Rest if class is provided
    if ONE_VS_REST_CLASS is not None:
        unique = cleaned_dataset[TARGET].unique()
        cleaned_dataset.loc[cleaned_dataset[TARGET].isin(unique[unique != ONE_VS_REST_CLASS]).values, TARGET] = 'REST'

    # handle low supported classes
    counts = cleaned_dataset[TARGET].value_counts()
    if DROP_LOW_SUPPORTED:
        cleaned_dataset = cleaned_dataset[~cleaned_dataset[TARGET].isin(counts[counts < MIN_CLASS_SUPPORT].index)]
    else:
        # group them
        cleaned_dataset.loc[
            cleaned_dataset[TARGET].isin(counts[counts < MIN_CLASS_SUPPORT].index).values, TARGET] = 'OTHER'

    # select target
    X = cleaned_dataset.drop([TARGET], axis=1)
    Y = cleaned_dataset[TARGET]

    # plot target distribution
    plot_target_distribution(Y, ex, TARGET, folder)

    # split train and test
    X_train_raw, X_test, y_train_raw, y_test = train_test_split(X, Y, test_size=0.2, random_state=30, stratify=Y)

    ## prepare train set
    raw_train_set = pd.concat([X_train_raw, y_train_raw], axis=1)

    # drop empty (any, all or below thresh)
    cleaned_train_set = raw_train_set.dropna(how=DROP_ROWS) if 'any' in DROP_ROWS or 'all' in DROP_ROWS \
        else raw_train_set.dropna(thresh=int(DROP_ROWS))

    # print cleaned train set statistics
    print_data_info(cleaned_train_set, cleaned=True)

    X_train = cleaned_train_set.drop([TARGET], axis=1)
    y_train = cleaned_train_set[TARGET]

    # apply smote (for imbalanced datasets)
    if SMOTE:
        sm = SMOTENC([X_train.columns.get_loc(x) for x in TRAIN_LABELS_CATEGORIAL], random_state=2, k_neighbors=pd.Index.value_counts(y_train).min()-1)
        X_train, y_train = sm.fit_sample(X_train, y_train)
        X_train = pd.DataFrame(X_train, columns=X.columns.values)
        y_train = pd.Series(y_train)

    ####################################         TARGET        #########################################################

    # encode categorial target
    le = LabelEncoder()
    le.fit(Y.fillna('MISSING'))
    y_train_transformed = le.transform(y_train.fillna('MISSING'))
    y_test_transformed = le.transform(y_test.fillna('MISSING'))

    ####################################         PIPELINE        #######################################################

    # prepare categorial pipeline
    cat_transformers = prepare_categorial_transformers(ENCODER, TRAIN_LABELS_CATEGORIAL)

    # prepare numerical pipeline
    num_transformers = prepare_numerical_transformers(TRAIN_LABELS_NUMERICAL)

    # put together categorial and numerical transformers
    transformers = cat_transformers + num_transformers
    ct = ColumnTransformer(transformers=transformers)

    # create classifier
    if 'Keras' in CLASSIFIER:
        num_input_neurons = get_num_input_neurons(ENCODER, TRAIN_LABELS_NUMERICAL, ct, X_train)
        classifier = get_keras_classifier(CLASSIFIER, num_input_neurons, num_out=len(np.unique(y_train_transformed)))
    elif CLASSIFIER == "FrequencyClassifier":
        classifier = FrequencyClassifier(col_idx=X[TRAIN_LABELS_CATEGORIAL + TRAIN_LABELS_NUMERICAL].columns.get_loc('from_' + TARGET[3:]))
    else:
        classifier = get_scikit_classifier(CLASSIFIER)

    # print params for CLASSIFIER
    print('Params for ' + CLASSIFIER)
    print(classifier.get_params())

    # building ML-pipeline
    ml_pipe = Pipeline([('transform', ct), ('ml-step', classifier)])

    ####################################     GRIDSEARCH      ###########################################################

    grid = None
    if GRIDSEARCH:
        grid = GridSearchCV(ml_pipe, param_grid=get_params(CLASSIFIER, NROWS), cv=5)

    ####################################   TRAIN - PREDICT   ###########################################################

    # create fit_params for Keras classifier
    fit_params = get_keras_callbacks() if 'Keras' in CLASSIFIER else {}

    if GRIDSEARCH:
        # train the ML-model (very time consuming!)
        grid.fit(X_train, y_train_transformed, **fit_params)

        # override unfitted pipe with fitted
        ml_pipe = grid.best_estimator_
        ct = ml_pipe.named_steps['transform']
    else:
        # train the ML-model (time consuming!)
        ml_pipe.fit(X_train, y_train_transformed, **fit_params)

    # predict
    raw_predictions = ml_pipe.predict(X_test)
    print(raw_predictions)

    ####################################    ANALYSE MODEL   ##########################################################

    # get estimator from pipe
    estimator = ml_pipe.named_steps['ml-step']

    # set categories
    categories = None
    if ENCODER == 'ohe' and hasattr(ct.named_transformers_['cat'].named_steps['ohe'], 'categories_'):
        categories = ct.named_transformers_['cat'].named_steps['ohe'].categories_

    # set feature labels
    feature_labels = get_feature_labels(ENCODER, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, categories)

    # analyse built in feature_importances (impurity)_ (eg. DecitionTreeClassifier)
    if hasattr(estimator, 'feature_importances_'):
        # plot top
        plot_top_feature_importances(ex, TARGET, estimator, feature_labels, folder, CLASSIFIER, ENCODER)
        # log and plot summed
        log_and_plot_feature_importances(ex, ENCODER, categories, estimator, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, TARGET, CLASSIFIER, folder)

    # analyse feature importance by permutation (more reliable)
    if PFIR:
        plot_and_log_permutation_feature_importance(ml_pipe, X_test, y_test_transformed, feature_labels, ENCODER, ex,
                                    TARGET, CLASSIFIER, folder, n_repeats=PFIR, n_jobs=1, groups=GROUPS.copy())

    # analyse coefs (weights) (eg. RidgeClassifier)
    if hasattr(estimator, 'coef_'):
        # plot
        plot_top_weights(ex, CLASSIFIER, TARGET, estimator, feature_labels, folder, ENCODER)
        # log and plot
        log_and_plot_weights(ex, ENCODER, TARGET, categories, estimator, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL,
                             CLASSIFIER, folder, target_classes=le.classes_)

    #############################   ANALYSE PREDICTION OF TEST SET   ###################################################

    # top
    prediction = le.inverse_transform(raw_predictions).reshape(-1, 1)

    # prediction probabilities
    predict_proba = None
    if hasattr(estimator, 'predict_proba'):
        predict_proba = estimator.predict_proba(ct.transform(X_test))
        print("probabilities")
        print(predict_proba)

    # prediction confidence scores
    if hasattr(estimator, 'decision_function'):
        print("INFO: Use decision_function for ranking.")
        conf_score = estimator.decision_function(ct.transform(X_test))
        # top 5
        top_idx_dec = (-conf_score).argsort()[:, :5]
        top_pred = le.classes_[top_idx_dec]
        top_pred_scores = np.array([conf_score[i, top_idx_dec[i]] for i in range(conf_score.shape[0])])
    elif hasattr(estimator, 'predict_proba'):
        print("INFO: Use predict_proba for ranking.")
        # top 5
        top_idx_dec = (-predict_proba).argsort()[:, :5]
        top_pred = le.classes_[top_idx_dec]
        top_pred_scores = np.array([predict_proba[i, top_idx_dec[i]] for i in range(predict_proba.shape[0])])
    elif CLASSIFIER == 'FrequencyClassifier':
        # there is only one top prediction
        N = prediction.shape[0]
        top_pred = np.c_[np.copy(prediction), np.copy(prediction), np.copy(prediction), np.copy(prediction), np.copy(prediction)]
        top_pred_scores = np.c_[np.ones(N), np.ones(N), np.ones(N), np.ones(N), np.ones(N)]
    else:
        top_pred = None
        top_pred_scores = None

    # plot (with numerical features only)
    if len(TRAIN_LABELS_NUMERICAL) == 2 and len(TRAIN_LABELS_CATEGORIAL) == 0:
        plot_classification_results(ml_pipe, TRAIN_LABELS_NUMERICAL, X, X_train, X_test, Y, y_train_transformed, y_test_transformed, TARGET)

    # plot/log precision/recall/F1-score/roc-score
    if hasattr(estimator, 'predict_proba') or hasattr(estimator, 'decision_function'):
        plot_precision_recall_F1(y_test_transformed, raw_predictions, le.classes_, ex, TARGET, folder, le,
                          y_score=estimator.decision_function(ct.transform(X_test)) if hasattr(estimator, 'decision_function') else estimator.predict_proba(ct.transform(X_test)),
                          title=TARGET + ' (' + CLASSIFIER + ')')

    # plot confusion matrix
    plot_confusion_matrix(y_test_transformed, raw_predictions, le.classes_, ex, TARGET, folder,
                          title=TARGET + ' - Absolute confusion matrix (' + CLASSIFIER + ')')

    plot_confusion_matrix(y_test_transformed, raw_predictions, le.classes_, ex, TARGET, folder, normalize=True,
                          title=TARGET + ' - Normalized confusion matrix (' + CLASSIFIER + ')')

    ####################################       PRINT        ###########################################################

    # prepare dimensions
    for i in range(5-top_pred_scores.shape[1]):
        top_pred_scores = np.c_[top_pred_scores, np.zeros(top_pred_scores.shape[0])]
        top_pred = np.c_[top_pred, np.zeros(top_pred.shape[0])]

    # concat data and print to file
    test_results_with_top_prediction = np.concatenate((X_test.get(TRAIN_LABELS_CATEGORIAL), X_test.get(TRAIN_LABELS_NUMERICAL),
                                                       pd.DataFrame(y_test), prediction, top_pred, top_pred_scores), axis=1)
    out_header = np.concatenate((TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, ['SOLL_' + TARGET, 'PRED_' + TARGET,
                                                                                   "pred_1", "pred_2", "pred_3",
                                                                                   "pred_4", "pred_5",
                                                                                   "pred_sc_1", "pred_sc_2",
                                                                                   "pred_sc_3", "pred_sc_4",
                                                                                   "pred_sc_5"]))
    np.savetxt(folder + TARGET + '_predictions.csv',
               np.append([out_header], test_results_with_top_prediction, axis=0), fmt='%s', delimiter=',')

    ####################################       EVALUTATE       ##########################################################

    if GRIDSEARCH:
        print("Best grid params: %s" % grid.best_params_)
        print("Best grid score on training data: %f" % grid.best_score_)
        return grid.best_score_
    elif "CV" in CLASSIFIER:
        print("Best alpha using built-in cross validation: %f" % estimator.alpha_)
        score_cross = ml_pipe.score(X_train, y_train_transformed)
        print("Best score on training data using built-in cross validation: %f" % score_cross)
        test_score = ml_pipe.score(X_test, y_test_transformed)
        print("Score on test data: %f" % test_score)
        return score_cross
    else:
        score_train = ml_pipe.score(X_train, y_train_transformed)
        print("Score on training data: %f" % score_train)
        test_score = ml_pipe.score(X_test, y_test_transformed)
        print("Score on test data: %f" % test_score)
        if 'Keras' in CLASSIFIER:
            print("Aborted cross validation since it is not supported for Keras")
            return test_score
        elif not CROSS_VALIDATION:
            return test_score
        elif score_train > 0.5:
            kf = KFold(n_splits=5, shuffle=True, random_state=123)
            # cross validation (very time consuming!)
            score_cross = cross_val_score(ml_pipe, X_train, y_train_transformed, cv=kf)
            print("Cross Validation scores: ")
            print(score_cross)
            print("Cross Validation mean score: %f" % score_cross.mean())
            return score_cross.mean()
        else:
            print("Aborted cross validation since train score is only: %f" % score_train)
            return score_train


if __name__ == '__run__':
    run()
