import bisect

from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder


class CustomLabelEncoder(BaseEstimator, TransformerMixin):

    def __init__(self):
        self.labelencoders = {}

    def fit(self, X, y=None):
        df_X = pd.DataFrame(X)
        for feature in df_X:
            df_Xn = pd.DataFrame({feature: df_X.loc[:, feature]})
            le = LabelEncoder()
            le.fit(df_Xn[feature])
            le_classes = le.classes_.tolist()
            bisect.insort_left(le_classes, '<unknown>')
            le.classes_ = le_classes
            self.labelencoders[feature] = le
        return self

    def transform(self, X, y=None):
        df_X = pd.DataFrame(X)
        data = None
        for feature in df_X:
            df_Xn = pd.DataFrame({feature: df_X.loc[:, feature]})
            le = self.labelencoders.get(feature)
            Xn = df_Xn[feature].map(lambda s: '<unknown>' if s not in le.classes_ else s)
            Xn_encoded = le.transform(Xn).reshape(-1, 1)
            if data is None:
                data = Xn_encoded
            else:
                data = np.concatenate((data, Xn_encoded), axis=1)
        return data
