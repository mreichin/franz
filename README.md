# Franz

Pre-Alpha-Version

## Introduction

This machine learning tool takes as input categorical and numerical features and can predict a single numerical or categorical target. The categorical target can be of `multi-class`, but the prediction will be a `single-label`. 
       
## Installation
Franz's requirements are the following:
* scikit-learn
* Keras
* numpy
* pandas
* sacred
* seaborn
* matplotlib


## Monitor
* Install and run `omniboard` with name `experiments`:

        omniboard -m localhost:27017:experiments

* Open in a browser: http://localhost:9000/
* Generated artifacts will be stored in the `results` folder.

## Configure
* Provide a csv-file with headers as first raw.
* Provide a yaml file as your configuration (see examples `config_cat.yaml` or `config_num.yaml` for details).
* To check if the config file will be loaded properly run:

        python predict_categorical.py print_config with config_cat.yaml
  or
  
        python predict_numerical.py print_config with config_num.yaml

## Run
* Run the experiment to predict a specific target. The  `TARGET_NAME` has to be one of the headers of your csv-file:
    * categorical target:
    
            python predict_categorical.py with config_cat.yaml --name=TARGET_NAME
        
    * numerical target:
    
            python predict_numerical.py with config_num.yaml --name=TARGET_NAME
            
* (Optional) Provide a specific parameter as an argument in order to ignore it from the config file e.g. provide the `MLPClassifier` as argument:

        python predict_categorical.py with config_cat.yaml 'CLASSIFIER=MLPClassifier' --name=TARGET_NAME
 