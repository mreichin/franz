from inspect import signature
from itertools import cycle
from math import floor, ceil

import numpy as np
import pandas as pd
import seaborn as sns
from keras.layers import Dense
from keras.models import Sequential
from keras.optimizers import SGD, Adam
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from scipy import interp
from sklearn.impute import SimpleImputer
from sklearn.inspection import permutation_importance
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, precision_score, recall_score, f1_score, \
    classification_report, precision_recall_curve, average_precision_score, cohen_kappa_score, roc_curve, auc
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder, label_binarize
from sklearn.utils import check_X_y
from sklearn.utils.multiclass import unique_labels
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_is_fitted, check_array
from rfpimp import importances, plot_importances

from CustomLabelEncoder import CustomLabelEncoder


def print_change_rate(TARGET, cleaned_dataset):
    if 'from_' + TARGET[3:] in cleaned_dataset.columns:
        from_idx = cleaned_dataset.columns.get_loc('from_' + TARGET[3:])
        to_idx = cleaned_dataset.columns.get_loc(TARGET)
        change_rate = np.sum(cleaned_dataset.iloc[:, from_idx] != cleaned_dataset.iloc[:, to_idx]) / cleaned_dataset.shape[0]
        print()
        print("{0:0.0f} % were changes to a different {1}.".format(change_rate * 100, TARGET[3:]))
        print()


def print_data_info(data, cleaned=False):
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    if not cleaned:
        print()
        print("raw data:")
        print(data)
        print()
    print(("Cleaned training data:" if cleaned else "Raw data:") + "[" + str(data.shape[0]) + " rows x " + str(data.shape[1]) + " columns]")
    print(("Cleaned training data:" if cleaned else "Raw data:") + " types")
    print(data.dtypes)
    print()
    print(("Cleaned training " if cleaned else "Raw ") + "data: statistics")
    print(data.describe().transpose())
    print()
    print(("Cleaned training data:" if cleaned else "Raw data:") + " null values")
    print(data.isna().sum())
    print()


def plot_target_distribution(Y, ex, TARGET, folder):
    value_counts = Y.value_counts()
    labels = value_counts.index
    frequency = value_counts.reset_index(drop=True)
    plt.figure(figsize=(10, 5))
    sns.barplot(frequency.index, frequency.values, alpha=0.8)
    plt.title('Frequency distribution of target ' + TARGET)
    if frequency.size > 50:
        plt.gca().axes.get_xaxis().set_visible(False)
    else:
        plt.xticks(rotation=90)
        plt.gca().set_xticklabels(labels)
    plt.ylabel('Number of Occurrences', fontsize=12)
    plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
    plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'target_distribution_' + TARGET + '.png', bbox_inches='tight')
    ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'target_distribution_' + TARGET + '.png')
    plt.show(block=False)


def prepare_numerical_transformers(TRAIN_LABELS_NUMERICAL):
    num_si_step = ('si', SimpleImputer(strategy='median'))
    num_ss_step = ('ss', StandardScaler())
    num_steps = [num_si_step, num_ss_step]
    num_pipe = Pipeline(num_steps)
    num_cols = TRAIN_LABELS_NUMERICAL
    num_transformers = [('num', num_pipe, num_cols)]
    return num_transformers


def prepare_categorial_transformers(ENCODER, TRAIN_LABELS_CATEGORIAL):
    cat_si_step = ('si', SimpleImputer(strategy='constant', fill_value='MISSING'))
    if ENCODER == 'le':
        cat_enc_step = (ENCODER, CustomLabelEncoder())
    elif ENCODER == 'ohe':
        cat_enc_step = (ENCODER, OneHotEncoder(sparse=False, handle_unknown='ignore'))
    else:
        raise ValueError('No categorial encoder set.')
    cat_steps = [cat_si_step, cat_enc_step]
    cat_pipe = Pipeline(cat_steps)
    cat_cols = TRAIN_LABELS_CATEGORIAL
    cat_transformers = [('cat', cat_pipe, cat_cols)]
    return cat_transformers


# Feature Importance

def plot_top_feature_importances(ex, TARGET, estimator, feature_labels, folder, REGRESSOR, ENCODER):
    print("feature_importances_ (decrease in impurity)")
    top = 5
    if ENCODER == 'le':
        top = len(feature_labels)
    print(estimator.feature_importances_[:top])
    plt.figure()
    paired_colors = plt.cm.Paired(range(top))  # Set3 or Pastel or Paired
    pd.Series(estimator.feature_importances_, index=feature_labels).nlargest(top).plot(kind='barh', color=paired_colors)
    plt.title(TARGET + ' (' + REGRESSOR + ', ' + ENCODER + ')')
    plt.xlabel('Feature Importance (Impurity)', size='large')
    plt.ylabel('Features (sorted)' if ENCODER == 'le' else 'Features (Top ' + str(top) + ')')
    plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
    plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'feature_importances_' + TARGET + '.png', bbox_inches='tight')
    ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'feature_importances_' + TARGET + '.png')
    plt.show(block=False)


def plot_and_log_permutation_feature_importance(estimator, X_test, y_test_transformed, feature_labels, ENCODER, ex,
            TARGET, CLASSIFIER, folder, n_repeats=10, n_jobs=2, groups=[]):
    #https://scikit-learn.org/dev/auto_examples/inspection/plot_permutation_importance.html#sphx-glr-auto-examples-inspection-plot-permutation-importance-py
    if groups:
        result = importances(estimator, X_test, pd.DataFrame(y_test_transformed), features=groups)
        viz = plot_importances(result, title=TARGET + ' (' + CLASSIFIER + ', ' + ENCODER + ')')
        viz.save(folder + 'run' + str(ex.current_run._id) + '_' + 'feature_importances_permutation_grouped_' + TARGET + '.svg')

    result = permutation_importance(estimator, X_test, y_test_transformed, n_repeats=n_repeats, n_jobs=n_jobs)
    print("Permutation Feature Importance (decreasing)")
    print(np.array2string(np.sort(result.importances_mean)[::-1], formatter={'float_kind':'{0:.4f}'.format}))
    print("Permutation Feature Importance (unsorted)")
    print(np.array2string(result.importances_mean[::-1], formatter={'float_kind':'{0:.4f}'.format}))
    sorted_idx = result.importances_mean.argsort()
    fig, ax = plt.subplots()
    ax.boxplot(result.importances[sorted_idx].T,
               vert=False, labels=X_test.columns[sorted_idx])
    ax.set_title(TARGET + ' (' + CLASSIFIER + ', ' + ENCODER + ')')
    ax.set_xlabel('Feature Importance (Permutation)', size='large')
    fig.tight_layout()
    plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
    plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'feature_importances_permutation_' + TARGET + '.png', bbox_inches='tight')
    ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'feature_importances_permutation_' + TARGET + '.png')
    plt.show(block=False)

def log_and_plot_feature_importances(ex, ENCODER, categories, estimator, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, TARGET,
                                     MODEL, folder):
    # log feature importances for every target-class (if ohe -> many columns)
    for x in range(estimator.feature_importances_.shape[0]):
        ex.log_scalar("feature_importances_", estimator.feature_importances_[x])

    # plot summed for 'ohe' and all for 'le'
    if ENCODER == 'ohe':
        if categories:
            # plot summed
            summed_importances = _get_and_log_summed_feature_importances(ex, categories, estimator)
            feature_labels_le = TRAIN_LABELS_CATEGORIAL + TRAIN_LABELS_NUMERICAL
            plt.figure()
            paired_colors = plt.cm.Paired(range(len(feature_labels_le)))  # Set3 or Pastel or Paired
            pd.Series(summed_importances, index=feature_labels_le).plot(kind='barh', color=paired_colors)
            plt.title(TARGET + ' (' + MODEL + ', ' + ENCODER + ', summed)')
            plt.xlabel('Feature Importance', size='large')
            plt.ylabel('Features')
            plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
            plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'feature_importances_summed' + TARGET + '.png', bbox_inches='tight')
            ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'feature_importances_summed' + TARGET + '.png')
            plt.show(block=False)


def _get_and_log_summed_feature_importances(experiment, categories, estimator):
    # log summed feature importances for categorial
    n = 0
    summed_importance = []
    for feature in range(len(categories)):
        importance = 0
        for _ in range(len(categories[feature])):
            importance = importance + estimator.feature_importances_[n]
            n = n + 1
        experiment.log_scalar("feature_importances_summed_", importance, feature)
        summed_importance.append(importance)

    # append numerical feature importances
    for x in range(n, estimator.feature_importances_.shape[0]):
        experiment.log_scalar("feature_importances_summed_", estimator.feature_importances_[x])
        summed_importance.append(importance)

    return summed_importance


# Weights

def plot_top_weights(ex, REGRESSOR, TARGET, estimator, feature_labels, folder, ENCODER):
    print("weights")
    print(estimator.coef_)
    top = 20
    if ENCODER == 'le':
        top = len(feature_labels)
    # plot top 12
    plt.figure()
    paired_colors = plt.cm.tab20(range(top))  # Set3 or Pastel
    pd.Series(np.sum(estimator.coef_, axis=0), index=feature_labels).nlargest(top).plot(kind='barh', color=paired_colors)
    plt.title(TARGET + ' (' + REGRESSOR + ', ' + ENCODER + ')')
    plt.xlabel('Weight', size='large')
    plt.ylabel('Features' if ENCODER == 'le' else 'Features (Top ' + str(top) + ')')
    plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
    plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'weights_' + TARGET + '.png', bbox_inches='tight')
    ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'weights_' + TARGET + '.png')
    plt.show(block=False)


def log_and_plot_weights(ex, ENCODER, TARGET, categories, estimator, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL,
                         MODEL, folder, target_classes):
    # TODO plot a single categorial target-class

    if ENCODER == 'le':
        # log weights for all target classes separate ( a single class if numerical target )
        coef = None
        if estimator.coef_.ndim == 1:  # eg. Lasso is not multiclass
            coef = estimator.coef_[np.newaxis]
        else:
            coef = estimator.coef_
        for x in range(coef.shape[0]):
            for y in range(coef[x].shape[0]):
                ex.log_scalar("weights_%s" % target_classes[x], coef[x, y])
    elif ENCODER == 'ohe':
        # sum over all target classes and ohe-features
        if categories:
            summed_weights = _get_and_log_summed_weights(ex, categories, estimator, target_classes)
            feature_labels_le = TRAIN_LABELS_CATEGORIAL + TRAIN_LABELS_NUMERICAL
            plt.figure()
            paired_colors = plt.cm.Paired(range(len(feature_labels_le)))  # Set3 or Pastel or Paired
            pd.Series(summed_weights, index=feature_labels_le).plot(kind='barh', color=paired_colors)
            plt.title(TARGET + ' (' + MODEL + ', ' + ENCODER + ', summed)')
            plt.xlabel('Weight', size='large')
            plt.ylabel('Features')
            plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
            plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'weights_summed' + TARGET + '.png', bbox_inches='tight')
            ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'weights_summed' + TARGET + '.png')
            plt.show(block=False)


def _get_and_log_summed_weights(experiment, categories, estimator, target_classes):
    coef = None
    if estimator.coef_.ndim == 1:  # eg. Lasso is not multiclass
        coef = estimator.coef_[np.newaxis]
    else:
        coef = estimator.coef_

    sum_of_sums = [0] * (len(categories) + (coef.shape[1] - sum(len(x) for x in categories)))
    for x in range(coef.shape[0]):
        # log summed feature weights for categorial
        n = 0
        for feature in range(len(categories)):
            weight = 0
            for _ in range(len(categories[feature])):
                weight += coef[x, n]
                n += 1
            experiment.log_scalar("weights_for_target_%s" % target_classes[x], weight, feature)
            sum_of_sums[feature] += weight
        # log numerical weights
        for i in range(n, coef.shape[1]):
            experiment.log_scalar("weights_for_target_%s" % target_classes[x], coef[x, i])
            sum_of_sums[len(categories) + i-n] += coef[x, i]
    for weight in sum_of_sums:
        experiment.log_scalar("weight_sums", weight)
    return sum_of_sums


def get_feature_labels(ENCODER, TRAIN_LABELS_CATEGORIAL, TRAIN_LABELS_NUMERICAL, categories):
    if ENCODER == 'le':
        return TRAIN_LABELS_CATEGORIAL + TRAIN_LABELS_NUMERICAL
    elif ENCODER == 'ohe':
        feature_labels = []
        for feature in range(len(categories)):
            for item in categories[feature]:
                feature_labels.append(TRAIN_LABELS_CATEGORIAL[feature] + '_' + item)
        feature_labels.extend(TRAIN_LABELS_NUMERICAL)
        return feature_labels
    else:
        raise ValueError('No categorial encoder set.')


def plot_classification_results(ml_pipe, TRAIN_LABELS_NUMERICAL, X, X_train, X_test, y, y_train_transformed, y_test_transformed, title):
    # original code from http://tinystruggles.com/2014/03/24/classification-with-scikit-learn.html
    plot_labels = TRAIN_LABELS_NUMERICAL[0:2]
    # Create color maps
    #cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
    #cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])
    cmap_light = plt.cm.get_cmap('Pastel1')
    cmap_bold = plt.cm.get_cmap('Set1')
    h = .02  # step size in the mesh
    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    x_min, x_max = X[plot_labels].values.astype(np.float32)[:, 0].min() - 1, X[plot_labels].values.astype(np.float32)[:, 0].max() + 1
    y_min, y_max = X[plot_labels].values.astype(np.float32)[:, 1].min() - 1, X[plot_labels].values.astype(np.float32)[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

    Z = ml_pipe.predict(pd.DataFrame({TRAIN_LABELS_NUMERICAL[0]: xx.ravel(), TRAIN_LABELS_NUMERICAL[1]: yy.ravel()}))

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap=cmap_light)

    # Plot also the training points
    plt.scatter(X_train[plot_labels].values.astype(np.float32)[:, 0], X_train[plot_labels].values.astype(np.float32)[:, 1], c=y_train_transformed, cmap=cmap_bold)

    y_predicted = ml_pipe.predict(X_test)
    score = ml_pipe.score(X_test, y_test_transformed)
    plt.scatter(X_test[plot_labels].values.astype(np.float32)[:, 0], X_test[plot_labels].values.astype(np.float32)[:, 1], c=y_predicted, alpha=0.5, cmap=cmap_bold)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.title(title)
    plt.show(block=False)


def plot_confusion_matrix(y_true, y_pred, classes, ex, TARGET, folder,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """ from example  https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html """
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # calculate frequency for sorting
    frequency = pd.Series(y_true).value_counts()

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred, frequency.index)

    # Only use the labels that appear in the data and sort by frequency
    classes = classes[unique_labels(y_true, y_pred)][frequency.index]

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print()
        print("Normalized confusion matrix")
    else:
        print()
        print('Confusion matrix, without normalization')
    print(cm)

    if len(classes) < 50:  # only makes sense with small number of classes
        fig, ax = plt.subplots(figsize=(len(classes)/1.6, len(classes)/1.6) if len(classes)/1.6 > 7 else (7, 7))
        im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
        ax.figure.colorbar(im, ax=ax)
        # We want to show all ticks...
        ax.set(xticks=np.arange(cm.shape[1]),
               yticks=np.arange(cm.shape[0]),
               # ... and label them with the respective list entries
               xticklabels=classes, yticklabels=classes,
               title=title,
               ylabel='True class',
               xlabel='Predicted class')

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center",
                        color="white" if cm[i, j] > thresh else "black")
        plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
        fig.tight_layout()
    else:
        fig, ax = plt.subplots(figsize=(23, 20))
        ax.matshow(cm, cmap=plt.get_cmap('viridis'))
        im = ax.imshow(cm, interpolation='nearest', cmap=plt.get_cmap('viridis'))
        ax.figure.colorbar(im, ax=ax)
        plt.title(title, fontsize=30)
        plt.xlabel('Predicted class', fontsize=20)
        plt.ylabel('True class', fontsize=20)
        plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=16)

    filename = folder + 'run' + str(ex.current_run._id) + '_' + 'conf_matrix'
    if normalize:
        filename = filename + '_normalised'
    filename = filename + '_' + TARGET + '.png'
    plt.savefig(filename, bbox_inches='tight')
    ex.add_artifact(filename)
    plt.show(block=False)


def plot_precision_recall_F1(y_true, y_pred, classes, ex, TARGET, folder, le,
                          y_score=None,
                          normalize=False,
                          title=None):
    print()
    print("precision micro: %f" % precision_score(y_true, y_pred, average='micro'))
    print()
    print("precision macro:  %f" % precision_score(y_true, y_pred, average='macro'))
    print()
    print("precision weighted:  %f" % precision_score(y_true, y_pred, average='weighted'))

    print()
    print("recall micro:  %f" % recall_score(y_true, y_pred, average='micro'))
    print()
    print("recall macro:  %f" % recall_score(y_true, y_pred, average='macro'))
    print()
    print("recall weighted:  %f" % recall_score(y_true, y_pred, average='weighted'))

    print()
    print("F1 score micro:  %f" % f1_score(y_true, y_pred, average='micro'))
    print()
    print("F1 score macro:  %f" % f1_score(y_true, y_pred, average='macro'))
    print()
    print("F1 score weighted:  %f" % f1_score(y_true, y_pred, average='weighted'))

    print()
    print("Cohens cappa: %f" % cohen_kappa_score(y_true, y_pred))

    print()
    print("Classification report: ")
    value_counts = pd.value_counts(y_true)
    print(classification_report(y_true, y_pred, target_names=le.inverse_transform(value_counts.index), labels=value_counts.index))

    # scatterplot classification report
    report = pd.DataFrame(classification_report(y_true, y_pred, target_names=le.inverse_transform(value_counts.index),
                                                output_dict=True, labels=value_counts.index))
    plotdata = report.drop(labels=['accuracy', 'macro avg', 'weighted avg'], axis=1).T.drop(labels=['support'], axis=1)
    if len(classes) < 50:
        plt.figure()
        plotdata.plot.line(linewidth=0.8)
        plt.title('Performance measures - ' + title)
        plt.xticks(rotation=90, ticks=np.arange(len(classes)), labels=le.inverse_transform(value_counts.index))
        plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
        plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'perform_measures_' + TARGET + '.png', bbox_inches='tight')
        ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'perform_measures_' + TARGET + '.png')
        plt.show(block=False)
    else:
        for measure in ['f1-score', 'recall', 'precision']:
            plt.figure()
            plt.scatter(x=np.arange(plotdata.shape[0]), y=plotdata[measure].values, s=2)
            plt.tick_params(labelbottom='off')
            plt.ylim(-0.05, 1.05)
            plt.grid(b=True, which='major', linewidth='0.2', linestyle='dotted')
            plt.title(measure + ' - ' + title)
            plt.ylabel(measure)
            plt.xlabel(TARGET + ' - sorted by frequency (decreasing from left to right)')
            plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
            plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + measure + '_measure_' + TARGET + '.png', bbox_inches='tight')
            plt.show(block=False)

    if y_score is not None:

        # modified code from scikit-learn docs https://scikit-learn.org/stable/auto_examples/model_selection/plot_precision_recall.html
        # and https://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html
        ######################################
        # precision recall curve
        precision = dict()
        recall = dict()

        if len(classes) == 2:
            # case binary classification
            precision, recall, _ = precision_recall_curve(y_true, y_score[:, 1])
            # In matplotlib < 1.5, plt.fill_between does not have a 'step' argument
            step_kwargs = ({'step': 'post'}
                           if 'step' in signature(plt.fill_between).parameters
                           else {})
            plt.figure()
            plt.step(recall, precision, color='b', alpha=0.2,
                     where='post')
            plt.fill_between(recall, precision, alpha=0.2, color='b', **step_kwargs)

            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.ylim([0.0, 1.05])
            plt.xlim([0.0, 1.0])
            plt.title('2-class Precision-Recall curve - ' + title + ': AP={0:0.2f}'
                      .format(average_precision_score(y_true, y_score[:, 1])))
            plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
            plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + '2_class_precision_score_' + TARGET + '.png',
                        bbox_inches='tight')
            ex.add_artifact(
                folder + 'run' + str(ex.current_run._id) + '_' + '2_class_precision_score_' + TARGET + '.png')
            plt.show(block=False)
        else:
            # case multi class
            average_precision = dict()
            y_true_bin = label_binarize(y_true, classes=np.arange(0, len(classes)))
            y_pred_bin = label_binarize(y_pred, classes=np.arange(0, len(classes)))

            # precision recall curve
            for i in range(len(classes)):
                precision[i], recall[i], _ = precision_recall_curve(y_true_bin[:, i],
                                                                    y_score[:, i])
                average_precision[i] = average_precision_score(y_true_bin[:, i], y_score[:, i])

            # A "micro-average": quantifying score on all classes jointly
            precision["micro"], recall["micro"], _ = precision_recall_curve(y_true_bin.ravel(),
                                                                            y_score.ravel())
            average_precision["micro"] = average_precision_score(y_true_bin, y_score,
                                                                 average="micro")
            print('Average precision score - micro-averaged over all classes: {0:0.2f}'
                  .format(average_precision["micro"]))
            print()

            # Plot the micro-averaged Precision-Recall curve
            # ...............................................
            #

            # In matplotlib < 1.5, plt.fill_between does not have a 'step' argument
            step_kwargs = ({'step': 'post'}
                           if 'step' in signature(plt.fill_between).parameters
                           else {})
            plt.figure()
            plt.step(recall['micro'], precision['micro'], color='b', alpha=0.2,
                     where='post')
            plt.fill_between(recall["micro"], precision["micro"], alpha=0.2, color='b',
                             **step_kwargs)

            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.ylim([0.0, 1.05])
            plt.xlim([0.0, 1.0])
            plt.title('Average precision score - ' + title + '\n micro-averaged over all classes: {0:0.2f}'
                  .format(average_precision["micro"]))
            plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
            plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'micro_av_precision_score_' + TARGET + '.png', bbox_inches='tight')
            ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'micro_av_precision_score_' + TARGET + '.png')
            plt.show(block=False)

            # Compute ROC curve and ROC area for each class
            fpr = dict()
            tpr = dict()
            roc_auc = dict()
            for i in range(len(classes)):
                fpr[i], tpr[i], _ = roc_curve(y_true_bin[:, i], y_score[:, i])
                roc_auc[i] = auc(fpr[i], tpr[i])

            # Compute micro-average ROC curve and ROC area
            fpr["micro"], tpr["micro"], _ = roc_curve(y_true_bin.ravel(), y_score.ravel())
            roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

            # Compute macro-average ROC curve and ROC area
            # First aggregate all false positive rates
            all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(classes))]))

            # Then interpolate all ROC curves at this points
            mean_tpr = np.zeros_like(all_fpr)
            for i in range(len(classes)):
                mean_tpr += interp(all_fpr, fpr[i], tpr[i])

            # Finally average it and compute AUC
            mean_tpr /= len(classes)

            fpr["macro"] = all_fpr
            tpr["macro"] = mean_tpr
            roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

            # plot micro and macro
            plt.figure()
            lw = 2
            plt.plot(fpr["micro"], tpr["micro"], lw=lw,
                     label='micro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["micro"]),
                     color='darkorange')

            plt.plot(fpr["macro"], tpr["macro"], lw=lw,
                     label='macro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["macro"]),
                     color='green')
            plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')
            plt.title('Receiver operating characteristic (ROC)  - ' + title)
            plt.legend(loc="lower right")
            plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)
            plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'av_roc_score_' + TARGET + '.png', bbox_inches='tight')
            ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'av_roc_score_' + TARGET + '.png')
            plt.show(block=False)

            if len(classes) < 50:
                ###############################################################################
                # Plot Precision-Recall curve for each class and iso-f1 curves
                # .............................................................
                #

                # setup plot details
                colors = cycle(plt.cm.get_cmap('tab10').colors)

                plt.figure(figsize=(12, 7))
                f_scores = np.linspace(0.2, 0.8, num=4)
                lines = []
                labels = []
                for f_score in f_scores:
                    x = np.linspace(0.01, 1)
                    y = f_score * x / (2 * x - f_score)
                    l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
                    plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

                lines.append(l)
                labels.append('iso-f1 curves')
                l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
                lines.append(l)
                labels.append('micro-average Precision-recall (area = {0:0.2f})'
                              ''.format(average_precision["micro"]))

                for i, color in zip(range(len(classes)), colors):
                    l, = plt.plot(recall[i], precision[i], color=color, lw=2)
                    lines.append(l)
                    if len(classes) < 22:
                        labels.append('{0} (area = {1:0.2f})'.format(classes[i], average_precision[i]))

                fig = plt.gcf()
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('Recall')
                plt.ylabel('Precision')
                plt.title('Precision-recall curves - ' + title)
                plt.legend(lines, labels, loc='center left', prop=dict(size=9))
                plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)

                plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'precision_recall_curves_' + TARGET + '.png', bbox_inches='tight')
                ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'precision_recall_curves_' + TARGET + '.png')
                plt.show(block=False)

                ###############################################################################

                # Plot Roc curve for each class (also micro and macro)
                plt.figure(figsize=(12, 7))
                plt.plot(fpr["micro"], tpr["micro"],
                         label='micro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["micro"]),
                         color='deeppink', linestyle=':', linewidth=4)

                plt.plot(fpr["macro"], tpr["macro"],
                         label='macro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["macro"]),
                         color='navy', linestyle=':', linewidth=4)

                for i, color in zip(range(len(classes)), colors):
                    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                             label='class {0} (area = {1:0.2f})'
                                   ''.format(classes[i], roc_auc[i]) if len(classes) < 22 else None)

                plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                plt.title('Roc curves - ' + title)
                plt.legend(loc="lower right", prop=dict(size=9))
                plt.figtext(.01, .01, "run_id = " + str(ex.current_run._id), fontsize=8)

                plt.savefig(folder + 'run' + str(ex.current_run._id) + '_' + 'roc_curves_' + TARGET + '.png', bbox_inches='tight')
                ex.add_artifact(folder + 'run' + str(ex.current_run._id) + '_' + 'roc_curves_' + TARGET + '.png')
                plt.show(block=False)

# Keras

class KerasTwoLayerFFClassifier(object):
    def __init__(self, num_in, num_out):
        self.num_in = num_in
        self.num_out = num_out

    def __call__(self):
        num_hidden = floor(np.mean([self.num_in, self.num_out]))
        clf = Sequential()
        clf.add(Dense(num_hidden, activation='relu', input_dim=self.num_in))
        clf.add(Dense(num_hidden, activation='relu'))
        clf.add(Dense(self.num_out, activation='softmax'))
        clf.compile(loss='categorical_crossentropy', optimizer=SGD(), metrics=["accuracy"])
        print(clf.summary())
        return clf


class KerasTwoLayerFFRegressor(object):
    def __init__(self, num_in):
        self.num_in = num_in

    def __call__(self):
        num_hidden = ceil(self.num_in / 2)
        clf = Sequential()
        clf.add(Dense(num_hidden, activation='relu', input_dim=self.num_in))
        clf.add(Dense(num_hidden, activation='relu'))
        clf.add(Dense(1, activation='linear'))
        clf.compile(loss='mean_squared_error', optimizer=Adam(), metrics=['mse', 'mae', 'mape', 'cosine'])
        print(clf.summary())
        return clf


def get_num_input_neurons(ENCODER, TRAIN_LABELS_NUMERICAL, ct, train):
    # this prefits the categorial column transformer - the assumption is, that a call on fit with the same
    # results training data results in the same encoding
    encoder = ct.fit(train).named_transformers_.get('cat').named_steps.get(ENCODER)
    if ENCODER == 'le':
        return len(encoder.labelencoders) + len(TRAIN_LABELS_NUMERICAL)
    elif ENCODER == 'ohe':
        return sum(len(x) for x in encoder.categories_) + len(TRAIN_LABELS_NUMERICAL)


class FrequencyClassifier(BaseEstimator, ClassifierMixin):
    """
    This Baseline Classifier predicts the to_feature by just predicting the most frequent to_feature
    for a specific from_feature

    Parameters
    ----------
    col_idx: The column index of the feature that has to be taken for the calculations. This has to
             be the same feature as the target. eg. to_ttkt needs the column from_ttkt
    """
    def __init__(self, col_idx=None):
        self.col_idx = col_idx

    def fit(self, X, y):
        """
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training input samples.
        y : array-like, shape (n_samples,)
            The target values. An array of int.
        Returns
        -------
        self : object
            Returns self.
        """
        # Check that X and y have correct shape
        X, y = check_X_y(X, y)

        # find majority class
        classes, indices = np.unique(y, return_inverse=True)
        majority_class = np.argmax(np.bincount(indices))

        # create list of most_frequent to_targets
        from_col = X[:, self.col_idx].ravel().astype(int)
        from_classes = np.unique(from_col)

        # initialize
        self.most_frequent_ = np.array([]).astype(int)
        found_missing = False

        for from_class in range(from_classes.max()+1):
            from_class_idx = np.where(from_col == from_class)[0]
            if len(from_class_idx) == 0:
                # <unknown> class -> map to majority class
                self.most_frequent_ = np.append(self.most_frequent_, majority_class)
                found_missing = True
            else:
                to_class_values = y[from_class_idx]
                to_classes, to_indices = np.unique(to_class_values, return_inverse=True)
                self.most_frequent_ = np.append(self.most_frequent_, to_classes[np.argmax(np.bincount(to_indices))])

        if not found_missing:
            # <unknown> class must be class with highest index e.g. [c1, c2, c3, <unknown>]
            self.most_frequent_ = np.append(self.most_frequent_, majority_class)

        return self

    def predict(self, X):
        """
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The input samples.
        Returns
        -------
        y : ndarray, shape (n_samples,)
            The predicted label for each sample.
        """
        # Check is fit had been called
        check_is_fitted(self, ['X_', 'y_'])

        # Input validation
        X = check_array(X)

        return self.most_frequent_[X[:, self.col_idx].ravel().astype(int)]